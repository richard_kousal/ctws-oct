*** Settings ***
Library  SeleniumLibrary
#Test Teardown  Close Browser

*** Variables ***
${URL}      https://marcel.veselka.gitlab.io/ctws-oct/
${HOST}     False
${BROWSER}  Chrome  # Firefox

${H1}       Hello World!

*** Test Cases ***
Check H1 Title
    [Tags]  smoke
    Open Browser  ${URL}  ${BROWSER}  remote_url=${HOST}  options=add_argument("--ignore-certificate-errors")
    Wait Until Page Contains  ${H1}
